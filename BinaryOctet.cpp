#include <iostream>
#include <assert.h>
#include "cmath"
#include "BinaryOctet.hpp"

char cStringArea[1024];

void BinaryOctet::make8Bit() {
    for (int i = 0; i < bitsPerOctet; i++) {
        char c = bitsAsDigits[i];
        if (c != '0' && c != '1') {
            bitsAsDigits[i] = '0';
        }
    }
}

bool BinaryOctet::isEven(BinaryOctet a) {
    int tmp = 0;
    for (auto c : a.bitsAsDigits) {
        if (c == '1') {
            tmp++;
        }
    }
    return (tmp % 2 == 0);
}

BinaryOctet addBinaryOctet(BinaryOctet a, BinaryOctet b) {
    BinaryOctet result = {};
    int aBit;
    int bBit;
    int sum;
    int carry = 0;
    for (int i = 7; i >= 0; i--) {
        aBit = (int) a.bitsAsDigits[i] - 48;
        bBit = (int) b.bitsAsDigits[i] - 48;
        sum = ((aBit ^ bBit) ^ carry);
        carry = ((aBit & bBit) | (aBit & carry)) | (bBit & carry);
        result.bitsAsDigits[i] =  (sum + 48);
    }
    return result;
}

BinaryOctet operator--(BinaryOctet a, int n) {
    BinaryOctet Substraktor = BinaryOctet::intToBinary(1);
    BinaryOctet tmp = BinaryOctet::zweierKomplement(a);
    return (Substraktor + tmp);
}


std::ostream &operator<<(std::ostream &os, const BinaryOctet &toBePrinted) {
    for (auto c : toBePrinted.bitsAsDigits) {
        os << c;
    }
    return os;
}

bool operator&&(BinaryOctet a, BinaryOctet b){
    for (int i = 0; i < sizeof(a.bitsAsDigits); ++i) {
      if(a.bitsAsDigits[i] == '0' || b.bitsAsDigits[i] == '0') {
            return false;
        }
    }
    return true;
}

BinaryOctet BinaryOctet::zweierKomplement(BinaryOctet a) {
    BinaryOctet tmp = {false, '0','0','0','0','0','0','0','1'};
    BinaryOctet tmpOctet;
    for (int i = 0; i < sizeof(a); ++i) {
        if(a.bitsAsDigits[i] == '0') {
            tmpOctet.bitsAsDigits[i] = '0';
        } else {
            tmpOctet.bitsAsDigits[i] = '1';
        }
    }
    return addBinaryOctet(tmp, tmpOctet);
}

BinaryOctet operator/(BinaryOctet a, BinaryOctet b) {
  return a;
}

BinaryOctet operator+(BinaryOctet a, int b) {
  BinaryOctet c = BinaryOctet::intToBinary(b);
  return addBinaryOctet(a, c);
}



int BinaryOctet::binaryToInt(BinaryOctet a) {
    int returnValue = 0;
    double exponent = 0;
    for (int i = bitsPerOctet - 1; i > 0; --i) {
        if (a.bitsAsDigits[i] == '0') {
            returnValue = returnValue + 0;
        } else if (a.bitsAsDigits[i] == '1') {
            returnValue = (int) (returnValue + pow(2.0,  exponent));
        }
        ++exponent;
    }

    return returnValue;
}

BinaryOctet operator/(BinaryOctet a, int b){
    int tmp = BinaryOctet::binaryToInt(a);
    int result = tmp / b;
    return BinaryOctet::intToBinary(result);
}

BinaryOctet BinaryOctet::intToBinary(int n) {
    int iterator = 0;
    int i = 0;
    int rest = 0;
    BinaryOctet binary;

    while (n > 0) {
        rest = n % 2;
        n = n / 2;

        i = bitsPerOctet - iterator - 1;
        binary.bitsAsDigits[i] = (rest == 0 ? '0' : '1');
        iterator++;
    }

    binary.evenParity = isEven(binary);

    return binary;
}

BinaryOctet operator+(BinaryOctet a, BinaryOctet b) {
    return addBinaryOctet(a, b);
}
