#include <iostream>
#include "BinaryOctet.hpp"

typedef BinaryOctet calctype;

//typedef int calctype;
//typedef unsigned int calctype;
//typedef double  calctype;



calctype doCalculation(calctype a, calctype b) {
    calctype result;

    for (; a && b; b--) {
        a = a + 1;
        a = a / 2;
    }

    result = a + b;

    return result;
}


int main(int argc, char **argv) {
    calctype a = {false, '0', '0', '0', '0', '1', '1', '0', '1'};
    calctype b = {false, '0', '0', '0', '0', '1', '0', '1', '1'};
    calctype result = doCalculation(a, b);


    //std::cout << addBinaryOctet(a, b);

    std::cout << std::boolalpha << "Parity: " << BinaryOctet::isEven(result) << std::endl;
    std::cout << "result = " << result << std::endl;

    return 0;
}
