#include <iostream>

const int bitsPerOctet = 8;

class BinaryOctet {
public:
    bool evenParity;
    char bitsAsDigits[bitsPerOctet];

    static bool isEven(BinaryOctet a);

    void make8Bit();

    static int binaryToInt(BinaryOctet a);

    static BinaryOctet intToBinary(int n);

    static BinaryOctet zweierKomplement(BinaryOctet a);

};

//BinaryOctet operator+=(BinaryOctet a, BinaryOctet b);

BinaryOctet intToDual(int n);

BinaryOctet addBinaryOctet(BinaryOctet a, BinaryOctet b);

BinaryOctet operator+(BinaryOctet a, int b);

BinaryOctet operator+(BinaryOctet a, BinaryOctet b);

BinaryOctet operator/(BinaryOctet a, int b);

bool operator&&(BinaryOctet a, BinaryOctet b);

BinaryOctet operator-(BinaryOctet a, BinaryOctet b);

BinaryOctet operator--(BinaryOctet a, int n);

std::ostream &operator<<(std::ostream &os, const BinaryOctet &toBePrinted);
